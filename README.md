# CI container images

Container images to use in CI pipelines.

To build and push an image:

```
podman build -t git.glasklar.is:5050/sigsum/admin/ci-container-images/<name> <name>
podman push git.glasklar.is:5050/sigsum/admin/ci-container-images/<name>
```

## trilliandb

A mysql container image to use with trillian.  This is a copy of the example
config in the trillian repo: https://github.com/google/trillian/tree/master/examples/deployment/docker/db_server.
